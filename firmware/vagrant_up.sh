#!/usr/bin/env bash
# https://github.com/vagrant-libvirt/vagrant-libvirt
vagrant up --provider=libvirt
