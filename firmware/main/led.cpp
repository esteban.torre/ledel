#include "led.hpp"
#include "esp32_digital_led_lib.h"
#include <esp_system.h>
#include <nvs_flash.h>
#include <stdio.h>
#include <driver/gpio.h>
#include <driver/uart.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <soc/rmt_struct.h>

#define HIGH 1
#define LOW 0
#define OUTPUT GPIO_MODE_OUTPUT
#define INPUT GPIO_MODE_INPUT

#define DEC 10
#define HEX 16
#define OCT 8
#define BIN 2

#define min(a, b)  ((a) < (b) ? (a) : (b))
#define max(a, b)  ((a) > (b) ? (a) : (b))
#define floor(a)   ((int)(a))
#define ceil(a)    ((int)((int)(a) < (a) ? (a+1) : (a)))
uint32_t IRAM_ATTR millis()
{
  return xTaskGetTickCount() * portTICK_PERIOD_MS;
}

void delay(uint32_t ms)
{
  if (ms > 0) {
    vTaskDelay(ms / portTICK_PERIOD_MS);
  }
}


#if DEBUG_ESP32_DIGITAL_LED_LIB
  int digitalLeds_debugBufferSz = 1024;
  char * digitalLeds_debugBuffer = static_cast<char*>(calloc(digitalLeds_debugBufferSz, sizeof(char)));
#endif

void gpioSetup(int gpioNum, int gpioMode, int gpioVal) {
    gpio_num_t gpioNumNative = static_cast<gpio_num_t>(gpioNum);
    gpio_mode_t gpioModeNative = static_cast<gpio_mode_t>(gpioMode);
    gpio_pad_select_gpio(gpioNumNative);
    gpio_set_direction(gpioNumNative, gpioModeNative);
    gpio_set_level(gpioNumNative, gpioVal);
}

strand_t STRANDS[] = { // Avoid using any of the strapping pins on the ESP32
  {.rmtChannel = 1, .gpioNum = 17, .ledType = LED_SK6812W_V1, .brightLimit = 255, .numPixels =  300,
   .pixels = nullptr, ._stateVars = nullptr},
};
int STRANDCNT = sizeof(STRANDS)/sizeof(STRANDS[0]);

void setup_leds()
{
  digitalLeds_initDriver();
  gpioSetup(16, OUTPUT, LOW);
  gpioSetup(17, OUTPUT, LOW);
  gpioSetup(18, OUTPUT, LOW);
  gpioSetup(19, OUTPUT, LOW);
  strand_t * strands[8];
  for (int i = 0; i < STRANDCNT; i++) {
    strands[i] = &STRANDS[i];
  }
  digitalLeds_addStrands(strands, STRANDCNT);
}

unsigned long random_color()
{
  unsigned long colors[] = {
    0xFF000000,
    0x00FF0000,
    0x0000FF00,
    0x000000FF,
    0xFFFFFFFF,
    0x00EEFF00,
    0x00EE00FF,
    0x0000FFFF,
    0x0070FF00,
    0x00E77700,
    0x000077FF,
    0x0000FF77,
    0x00D0FFB9,
    0x00C044D9,
    0x007077FF,
    0x0050FFB9,
    0x990000FF,
    0x330000FF,
  };
  return colors[esp_random()%18];
}

void random_strand(strand_t * p_strand)
{
  for (uint16_t i = 0; i < p_strand->numPixels; i++) {
    int xxx=esp_random()%49;
    if(xxx<2)
      p_strand->pixels[i].num = random_color();
    else if(xxx<40)
	    p_strand->pixels[i].num = 0;
  }
}

void random_strands(strand_t * strands[], int numStrands)
{
  for (int n = 0; n < numStrands; n++) {
    strand_t * pStrand = strands[n];
	  random_strand(pStrand);
  }
  digitalLeds_drawPixels(strands, numStrands);
}

void run_leds()
{ 
  strand_t * strands [STRANDCNT];
  for (int i = 0; i < STRANDCNT; i++) {
    strands[i] = &STRANDS[i];
  }
	random_strands(strands,STRANDCNT);
}
