#ifndef LEDS_H
#define LEDS_H
#ifdef __cplusplus
extern "C"{
void setup_leds();
void run_leds();
}
#else
extern void setup_leds();
extern void run_leds();
#endif
#endif
